<?php

namespace App\Http\Controllers\Fame;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FameServiceController extends Controller {

    private $listCount;

    public function __construct() {
        $this->listCount = rand(50, 100);
    }

    /**
     * DB: 'INDO_BOP',
     * SS: 'tot',
     * ENV: 'Dev',
     * SDB: 'INDO_BOP_SCRN',
     * SON: 'DESC.EN',
     * @return List<FameObject>
     */
    public function getFameObjectByContextWithMetaData() {
        return $this->getFameObjectWithMetaData();
    }

    /**
     * ENV: 'Dev',
     * DB: 'INDO_BOP',
     * OC: 'search query',
     * @return List<FameObject>
     */
    public function getFameObjectWithMetaData() {
        $result = [];

        for ($i = 0; $i < $this->listCount; $i++) {
            $fameObject = new FameObject();
            $fameObject->ObjectName = str_random(20);
            $fameObject->Description = str_random(20);

            $result[] = $fameObject;
        }

        return $result;
    }

    private function generateTiqObjectName($series, $freq = "M") {
        return "EXIM_BI_1_1_M.M.010700.00.01.02.USD.OBS_VALUE";

        $keys = ["$series_$freq", $freq];
        $keys[] = "010".rand(100, 999);
        $keys[] = str_pad(rand(0, 99), 2, '0', STR_PAD_LEFT);
        $keys[] = str_pad(rand(0, 99), 2, '0', STR_PAD_LEFT);
        $keys[] = str_pad(rand(0, 99), 2, '0', STR_PAD_LEFT);
        $keys[] = "USD";
        $keys[] = "OBS_VALUE";

        return implode(".", $keys);
    }

    /**
     * DB: 'INDO_BOP',
     * ENV: 'Dev',
     * OBS: 'EXIM_BI_1_1_M.M.010700.00.01.02.USD.OBS_VALUE',
     * OBSERVE: 'END',
     * START: '04/01/2011',
     * END: '12/31/2011',
     * @return List<CustomTiq>
     */
    public function getTiqObjects() {
        $result = [];

        for ($i = 0; $i < $this->listCount; $i++) {
            $customTiq = new CustomTiq();
            $customTiq->Name = $this->generateTiqObjectName("EXIM_BI_1_1");
            // $customTiq->Name = "EXIM_BI_1_1_M.M.010700.00.02.02.USD.OBS_VALUE";
            $customTiq->Description = str_random(10);

            for ($j = 10; $j >= 0; $j--) {
                $obs = new Observation();
                $obs->DateIndex = date('Y') - $j;
                $obs->ObservationValue = rand(0, 1000);

                $customTiq->Observations[] = $obs;
            }

            $result[] = $customTiq;
        }

        return $result;
    }

    public function getDsd(Request $req) {
        $dsd_parts = explode(".", $req->dsd);
        $dsd_id = current($dsd_parts);
        $dsd_file = storage_path("app/sdmx/sdmx_dsd_${dsd_id}.xml");

        return response()->download($dsd_file);
    }
}

class FameObject {
    public $ObjectName;
    public $ObjectType;
    public $Frequency;
    public $StartDate;
    public $EndDate;
    public $DataType;
    public $Observed;
    public $FAMEpartition;
    public $KeyFamilyId;
    public $Documentation;
    public $Description;
    public $CollectionIndicator;
    public $MultiplierUnit;
    public $Decimaldiffusion;
    public $Lag;
    public $privateationCalendar;
    public $Coverage;
    public $QualityStandard;
    public $NoteMethodology;

    public $Basis;
    public $Calendar;

    public $Currency;
    public $Source;
    public $Unit;
    public $ArabicDescription;
    public $FormulaExpression;
    public $SdmxName;
}

class CustomTiq {
    public $Name; // Object Name
    public $Type; // Formula/Series/Scalar
    public $Description; // Object Description
    public $Documentation; // Object Documentation

    /**
     * @return List<Observation>
     */
    public $Observations = []; // Data Point Collection
}

class Observation {

    public $DateIndex; // Index of an object in fame date format

    /**
     * @return  Object
     */
    public $ObservationValue; // Value of the object

    /**
     * @return  Object
     */
    public $OldValue = 0; // Old value of the objects (not being used)
}
