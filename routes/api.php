<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::any("rest/sdmx.aspx", "Fame\FameServiceController@getDsd");

Route::prefix("v1")->middleware("api")->group(function () {
    Route::get("reports", "ReportController@getReportList");
    Route::get("report/desc/{path}", "ReportController@getResourceDescriptor")->where("path", "(.*)");
    Route::get("report/export/{type}", "ReportController@export");

    Route::get("category/{source}", "CategoryController@find");
});

Route::get("rest", function() {
    return "OK";
});

Route::prefix("rest/GetFameObjectsService")->group(function() {
    Route::any("GetTiqObjects", "Fame\FameServiceController@getTiqObjects");
    Route::any("GetFameObjectWithMetaData", "Fame\FameServiceController@getFameObjectWithMetaData");
    Route::any("GetFameObjectByContextWithMetaData", "Fame\FameServiceController@getFameObjectByContextWithMetaData");
    // Route::any("sdmx.aspx?dsd=EXIM_BI_1_1_M", "Fame\FameServiceController@getDsd");
});